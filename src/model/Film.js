export default class Film {

    constructor(json) {
        this.id=json.id;
        this.title=json.title;
        this.vote_average=json.vote_average;
        this.overview=json.overview;
        this.image="https://image.tmdb.org/t/p/w500"+json.poster_path;
        this.annee=json.release_date;
        this.commentaire="";
        this.note=0;
        this.acteur="";
        this.realisateur="";
    }
    setacteur(acteur){
        this.acteur=acteur;
    }
    setrealisateur(realisateur){
        this.realisateur=realisateur;
    }


}
